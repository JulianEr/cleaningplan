
# -*- coding: utf-8 -*-

from subprocess import call
import datetime
import locale
import sys
import argparse

locale.setlocale(locale.LC_ALL, "de_DE.utf8")

parser = argparse.ArgumentParser()

def getDateFromString(s):
    return datetime.datetime.strptime(s, '%d.%m.%Y')

def getLastMonday(date=datetime.date.today()):
    return date - datetime.timedelta(days=date.weekday())

parser.add_argument('--fromDate', type=lambda s: getLastMonday(getDateFromString(s)), default=getLastMonday(), help="If not provided the last monday from today will be used. If provided, the last monday from the given date will be used. Use the format dd.mm.YYYY.")
parser.add_argument('--listOfPersons', nargs="+", required=True, help="The list of persons participating in the schedule (mandatory; got to be as long as the list of rooms).")
parser.add_argument('--listOfPlaces', nargs="+", required=True, help="The list of rooms which are to tidy up (mandatory; got to be as long as the list of participants).")
parser.add_argument('--lineHeight', type=lambda s: -float(s), default=-0.575, help="The height of lines inside each box. Default is 0.575.")
parser.add_argument('--checkboxWidth', type=float, default=0.35, help="The width of the check box next to each name and room pait. Default is 0.35.")
parser.add_argument('--boxWidth', type=float, default=6.0, help="The total box width including the checkbox. Default is 6.0 for two boxes each line. Set it to 4.5 for three boxes per line.")
parser.add_argument('--dateFormat', type=str, default='%d.%m.%Y', help="The format for the dates in the first line of each box. Use the usual format specifier, i. e. %%d, %%m, and %%Y. Default is \"%%d.%%m.%%Y\". If three boxes should be used in each line consider using \"%%d.%%m\".")
parser.add_argument('--title', type=str, default="Putzplan", help="The title of the cleaning plan. Default is Putzplan.")
parser.add_argument('--compile', action='store_true', help="If set the .tex file will be compiled with standard pdf-Latex. Note that pdf-Latex has to be installed and accessible in the PATH variable.")

args = parser.parse_args()

assert(len(args.listOfPersons) == len(args.listOfPlaces))

max_textwidth = 14.6
max_textheight = 19.4

sixDays = datetime.timedelta(days=6)
nextWeek = datetime.timedelta(weeks=1)

linesPerBox = len(args.listOfPersons) + 1

boxHeight = (linesPerBox*abs(args.lineHeight))
lines = int(max_textheight/boxHeight)
verticalSpaceBetweenBoxes = (max_textheight-lines*boxHeight)/float(lines-1)


line_heigh = args.lineHeight
checkbox_width = args.checkboxWidth
checkbox_margin = 0.5 * (-line_heigh - checkbox_width)

date_width = args.boxWidth - checkbox_width - 2.0*checkbox_margin
name_width = date_width/2.0
place_width = date_width/2.0

if checkbox_margin < 0.0:
    raise RuntimeError("The checkbox is bigger than the line. I can not allow this.")

checkbox_start = date_width + checkbox_margin

boxesInLine = int(max_textwidth/args.boxWidth)
horizontalSpaceBetweenBoxes = (max_textwidth-boxesInLine*args.boxWidth)/float(boxesInLine) if boxesInLine > 1 else 0.0

with open("Plan.tex", "w") as ofile:


    def write_prae(ofile):
        ofile.write("\\documentclass[a4paper, ]{scrartcl}\n")
        ofile.write("\\usepackage[T1]{fontenc}\n")
        ofile.write("\\usepackage[utf8]{inputenc}\n")
        ofile.write("\\usepackage{lmodern}\n")
        ofile.write("\\usepackage{eurosym}\n")
        ofile.write("\\usepackage{color}\n")
        ofile.write("\\usepackage{tikz}\n\n")
        ofile.write("\\usepackage{titlesec}\n")
        ofile.write("\\titleformat{\section}[block]{\\Huge\\bfseries\\filcenter}{}{1em}{}\n")
        ofile.write("\\begin{document}\n")
        ofile.write("\\pagestyle{empty}\n")
        ofile.write("\\begin{center}\n")
        ofile.write(f"\\section*{{{args.title}}}\n")
        ofile.write("\\begin{tikzpicture}\n")
    
    def dateSpan(date):
        return "{:s} - {:s}".format(date.strftime(args.dateFormat), (date + sixDays).strftime(args.dateFormat))
	
    def write_entries(ofile):
        currentDate = args.fromDate
        boxCount = 0
        def coordLine(lineNumber, whichBar):
            return (lineNumber*linesPerBox + whichBar) * line_heigh - lineNumber * verticalSpaceBetweenBoxes
    
        def makeOneBoxFrom(oFile, start, date, boxCount):
            
            for i in range(1, len(args.listOfPersons)+1):
                lineHeight = coordLine(lineNumber, i)
                ofile.write(f"\\draw({start:.3f},{lineHeight:.3f}) -- ({date_width+start:.3f},{lineHeight:.3f});\n")
                ofile.write(f"\\draw ({checkbox_start+start:.3f},{lineHeight-checkbox_margin:.3f}) -- ({checkbox_start + checkbox_width +start:.3f},{lineHeight-checkbox_margin:.3f}) -- ({checkbox_start + checkbox_width +start:.3f},{lineHeight-checkbox_margin-checkbox_width:.3f}) -- ({checkbox_start+start:.3f},{lineHeight-checkbox_margin-checkbox_width:.3f}) -- cycle;\n")
                ofile.write(f"\\node at ({0.5*name_width+start:.3f}, {lineHeight+0.5*line_heigh:.3f}) {{ {args.listOfPersons[(boxCount + i - 1) % len(args.listOfPersons)]} }};\n")
                ofile.write(f"\\node at ({name_width+0.5*place_width+start:.3f}, {lineHeight+0.5*line_heigh:.3f}) {{ {args.listOfPlaces[i-1]} }};\n")

            ofile.write(f"\\draw({start+date_width/2.0:.3f},{coordLine(lineNumber, 1):.3f}) -- ({start+date_width/2.0:.3f},{coordLine(lineNumber, len(args.listOfPersons)+1):.3f});\n")
            ofile.write(f"\\draw ({start:.3f},{coordLine(lineNumber, 0):.3f}) -- ({date_width+start:.3f},{coordLine(lineNumber,0):.3f}) -- ({date_width+start:.3f},{coordLine(lineNumber, len(args.listOfPersons)+1):.3f}) -- ({start:.3f},{coordLine(lineNumber, len(args.listOfPersons)+1):.3f}) -- cycle;\n")
			
            ofile.write(f"\\node at ({0.5*date_width+start:.3f}, {coordLine(lineNumber, 0)+0.5*line_heigh:.3f}) {{ {date} }};\n")

        for lineNumber in range(lines):
            start = horizontalSpaceBetweenBoxes*0.5
            for _ in range(boxesInLine):
                makeOneBoxFrom(ofile, start, dateSpan(currentDate), boxCount)
                start += args.boxWidth + horizontalSpaceBetweenBoxes
                boxCount += 1
                currentDate += nextWeek

        ofile.write("\\end{tikzpicture}\n")
        ofile.write("\\end{center}\n")
        ofile.write("\\end{document}\n")

    write_prae(ofile)
    #write_head(ofile)
    write_entries(ofile)

if args.compile:
    call(["pdflatex", "Liste.tex"])

